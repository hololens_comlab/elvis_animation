﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Riconoscimenti : MonoBehaviour
{
    public float interpolationValue;
    public int delayTime;

    private List<Image> immagini;
    private Text textWidget;
    private bool sceneEnded;
    private float alpha;

    void Start()
    {
        immagini = new List<Image>(this.GetComponentsInChildren<Image>());
        textWidget = this.GetComponentInChildren<Text>();
        sceneEnded = false;

        alpha = interpolationValue;
    }

    // Update is called once per frame
    void Update()
    {
        if (sceneEnded)
        { 
            foreach (Image image in immagini)
            {
                FadeIn(image);
            }

            FadeIn(textWidget);
        }
    }

    private void FadeIn( MaskableGraphic element )
    {
        Color newColor = element.color;
        newColor.a = (1 - alpha) * element.color.a + alpha;
        element.color = newColor;
        print(element.color);
    }

    public IEnumerator show()
    {
        yield return new WaitForSeconds(delayTime);
        sceneEnded = true;
    }
}
