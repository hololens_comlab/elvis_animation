﻿using UnityEngine;

public class Timer : MonoBehaviour
{
    private float timeLeft;
    private IListener listener;

    public void SetStartTime(float startTime) { this.timeLeft = startTime; }
    public void setListener(IListener listener) { this.listener = listener; }

    void Update()
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            listener.timeEnded();
            Destroy(this);
        }
    }

}
