﻿using UnityEngine;

public class SequenceElement
{
    private readonly string text;
    private readonly AudioClip audioClip;
    private readonly float duration; //in secondi

    public SequenceElement(string text, AudioClip audioClip, float duration)
    {
        this.text = text;
        this.audioClip = audioClip;
        this.duration = duration;
    }

    //public void SetText(string text) { this.text = text; }
    public string GetText() { return text; }

    //public void SetAudioClip(AudioClip audioClip) { }
    public AudioClip GetAudioClip() { return audioClip; }

    //public void SetDuration(float duration) { this.duration = duration; }
    public float GetDuration() { return duration; }

}
