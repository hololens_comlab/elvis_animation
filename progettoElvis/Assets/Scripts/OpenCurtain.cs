using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCurtain : MonoBehaviour
{
    // Start is called before the first frame update
    
    [SerializeField]
    private float speed = 0.2f;
    [SerializeField]
    private float maxDistance = 1f;

    private float totalDistance;

    private Transform leftCurtain;
    private Transform rightCurtain;

    void Start()
    {
        totalDistance = 0;

        Transform localCurtain = transform.GetChild(0);
        if (localCurtain.position.x > 0)
        {
            rightCurtain = localCurtain;
            leftCurtain = transform.GetChild(1);
        }
        else
        {
            leftCurtain = localCurtain;
            rightCurtain = transform.GetChild(1);
        }
    }

    void moveCurtain(Transform transform, float increment)
    {
        if (transform == null)
        {
            return;
        }

        if (totalDistance < maxDistance)
        {
            Vector3 movement = transform.position + transform.right * increment;
            transform.SetPositionAndRotation(movement, transform.rotation);
        }
        else
        {
            //soluzione migliore: far passare alpha da 1 a 0 con una transizione
            Destroy(transform.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        float increment = speed * Time.deltaTime;
        totalDistance += increment; //salvo i metri percorsi dalla tenda

        moveCurtain(leftCurtain, increment);//nota siccome ho girato di 180, right � left in realt�
        moveCurtain(rightCurtain, -increment);
    }
}
