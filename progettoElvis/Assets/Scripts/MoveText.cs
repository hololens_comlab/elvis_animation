﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveText : MonoBehaviour
{
    public Text textTarget;

    private Animator animator;
    private string userText;

    private void Start()
    {
        animator = this.GetComponentInParent<Animator>();
        userText = textTarget.text;
        textTarget.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = Camera.main.WorldToScreenPoint(this.transform.position);
        textTarget.transform.position = newPos;

        bool showText = animator.GetBool("ShowText");
        bool canTalk = animator.GetBool("CanTalk");

        if (showText)
        {
            Invoke("ShowText", .5f);
        }
        else
        {
            textTarget.text = "";
        }

        if (canTalk)
        {
            textTarget.text = "";
            Destroy(this);
        }
    }

    private void ShowText()
    {
        textTarget.text = userText;
    }
}
