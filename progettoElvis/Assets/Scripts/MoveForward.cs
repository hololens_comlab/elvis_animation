using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour
{
    public float speed;
    //public float timeAnimationSamba = 5.0f;
    public float targetZPosition;
    public float startZPosition;
    public bool forwardDirection;

    public float rotationRateo;
    public float timeInterval; //ogni quante volte effettuare una rotazione


    private Animator animator;
    private bool canMoveBack;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        Vector3 startPosition = transform.position;
        startPosition.z = startZPosition;

        if (!forwardDirection)
        {
            StartCoroutine(FadeRotation());
        }
        transform.SetPositionAndRotation( startPosition, transform.rotation );
    }

    IEnumerator FadeRotation()
    {
        print(transform.rotation.eulerAngles);
        yield return new WaitForSeconds(1);

        float totalAngle = 0;
        while (totalAngle <= 180)
        {
            float rotation = rotationRateo;
            transform.Rotate(new Vector3(0, -rotation, 0));
            totalAngle = totalAngle + rotation;
            yield return new WaitForSeconds(timeInterval);
        }

        canMoveBack = true;

    }

    // Update is called once per frame
    void Update()
    {
        if (forwardDirection)
        {
            if (transform.position.z > targetZPosition)
            {
                Vector3 movement = transform.position + transform.forward * speed * Time.deltaTime;
                transform.SetPositionAndRotation(movement, transform.rotation);
            }
            else
            {
                Destroy(this);
            }
        }
        else
        {
            if (transform.position.z < targetZPosition && canMoveBack)
            {
                Vector3 movement = transform.position + transform.forward * speed * Time.deltaTime;
                transform.SetPositionAndRotation(movement, transform.rotation);
            }
            else if( transform.position.z >= targetZPosition)
            {
                animator.SetBool("ElvisFall", true);
                Destroy(this);
            }
        }
    }
}
