﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public interface IListener
{
    void timeEnded();
}

public class SceneController : MonoBehaviour, IListener
{
    public AudioSource audioSource;
    public Text subtitleContainer;
    public Image background;
    public Animator animator;
    public GameObject elvis;
    public Riconoscimenti riconoscimenti;

    private List<SequenceElement> sequence = new List<SequenceElement>();
    private bool isTalking; // se sta effettivamente parlando mostro i sottotitoli
    private int currentText;
    private bool canTalk; //indica se è partita l'animazione di Elvis che parla
    private bool endPresentation;
    

 
    // Start is called before the first frame update
    void Start()
    {
        loadTemporaryText();
        currentText = 0;

        //NextSequenceElement();
        isTalking = false; // per non fare uscire subito il background
                           // si può sistemare forse inserendo il campo talking nel SequenceElement
        background.color = Color.clear;
        canTalk = false;
        endPresentation = false;
        animator.SetBool("CanTalk", canTalk);
        subtitleContainer.text = "";
    }

    private void loadTemporaryText()
    {
        AudioClip audioclip1 = Resources.Load<AudioClip>("Audio/audio1");
        AudioClip audioclip2 = Resources.Load<AudioClip>("Audio/audio2");
        AudioClip audioclip3 = Resources.Load<AudioClip>("Audio/audio3");
        AudioClip audioclip4 = Resources.Load<AudioClip>("Audio/audio4");



        //forse è meglio gestire da un file, ma sono troppo pigro per farlo adesso ;-)
        //sequence.Add(new SequenceElement("", null, 3)); //pausa iniziale, forse non serve se il testo parte insieme all'animazione
        sequence.Add(new SequenceElement("Elvis è tornato!", audioclip1, 2 ));
        sequence.Add(new SequenceElement("Ti inviterei a ballare con me...", audioclip2, 3));
        sequence.Add(new SequenceElement("Ma ancora non sei iscritto a Ingegneria Elettronica!", audioclip3, 3));
        sequence.Add(new SequenceElement("Che aspetti?", audioclip4, 2));
    }

    // Update is called once per frame
    void Update()
    {
        canTalk = animator.GetBool("CanTalk");
        bool elvisFall = animator.GetBool("ElvisFall");

        if (elvisFall)
        {
            //riconoscimenti.show();
            StartCoroutine(riconoscimenti.show());
        }

        if (!canTalk)
        {
            //inutile andare avanti finchè non parte l'animazione
            HideUI();
            return;
        }


        if (isTalking)
        {
            ShowUI();
        }
        else
        {
            HideUI();

            if (currentText < sequence.Count)
            {
                NextSequenceElement();
            }
        }
    }

    private void HideUI()
    {
        subtitleContainer.text = "";
        Color color = Color.Lerp(background.color, Color.clear, 0.1f);
        background.color = color;
    }

    private void ShowUI()
    {
        Color color = Color.Lerp(background.color, Color.black, 0.1f);
        background.color = color;
    }

    private void addNewTimer( SequenceElement element )
    {
        Timer timer = this.gameObject.AddComponent<Timer>();
        timer.setListener(this);
        timer.SetStartTime(element.GetDuration());
    }

    private void NextSequenceElement()
    {
        SequenceElement element = sequence[currentText];
        //se non ho un nuovo audioclip continuo ad eseguire il precedente o non faccio niente
        if (element.GetAudioClip() != null)
        {
            audioSource.clip = element.GetAudioClip();
            audioSource.Play();
        }
        subtitleContainer.text = element.GetText();
        addNewTimer(element);
        isTalking = true;
    }

    public void timeEnded()
    {
        //vai avanti e imposta altro testo
        currentText += 1;
        if (currentText < sequence.Count)
        {
            //NextSequenceElement();
        }
        else
        {
            //fine presentazione
            canTalk = false;
            endPresentation = true;
            animator.SetBool("CanTalk", canTalk);
            animator.SetBool("EndPresentation", endPresentation);

            SetElvisBackMovement();

            audioSource.Stop();
            audioSource.clip = null;

        }
        isTalking = false;
    }

    private void SetElvisBackMovement()
    {
        elvis.AddComponent<MoveForward>();
        //aggiungo componente per movimento
        MoveForward move = elvis.GetComponent<MoveForward>();
        //imposto i parametri
        move.forwardDirection = false;
        move.startZPosition = 3;
        move.targetZPosition = 6;
        move.speed = 0.5f;
        move.rotationRateo = 3f;
        move.timeInterval = 0.05f;
    }

    public void SceneRestart()
    {
        SceneManager.LoadScene("StartScene");
    }
}
