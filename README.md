# Consigli

- Usare la versione più recente di Unity, però se volete usare il MixedRealityToolkit vi conviene la versione 2018.3.x (altrimenti non vi funzionerà)
- Le librerie variano con alta frequenza ( molte cose vengono spesso deprecate dopo pochi mesi )
- Non usare .Net come compilatore ma Cpp, poichè in .Net è quasi tutto deprecato
- Hololens non è molto performante, non appesantire troppo il programma, lasciare le "cose pesanti" ad un server



# Utility

https://github.com/Microsoft/MixedRealityToolkit-Unity

# Struttura cartelle

Le cartelle principali sono Animation e Script all'interno della cartella Assets. All'interno troverete il 90% delle funzionalità 
della presentazione 3D. Per altri dettagli vi conviene vedere la macchina a stati relativa all'animator di Elvis in Unity.

# Aggiungere/Modificare il testo della presentazione 
In SceneController è possibile aggiungere nuove frasi con i relativi audio. Per ora bisogna per forza modificare il codice per aggiungere nuovi elementi.
Però l'operazione è abbastanza semplice, basta aggiungere un nuovo elemento alla lista 'sequence' con il metodo Add.

All'interno del metodo basta passare una nuova istanza di SequenceElement con la tripla <testo, audioclip, durata>